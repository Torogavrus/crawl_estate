# -*- coding: utf-8 -*
from invoke import task

provinces_list = [u"安徽", u"北京", u"重庆", u"福建", u"甘肃", u"广东", u"广西", u"贵州", u"海南", u"河北", u"黑龙江",
                  u'河南', u'湖北', u'湖南', u'江苏', u'江西', u'吉林', u'辽宁', u'内蒙古', u'宁夏', u'青海', u'上海',
                  u'山东', u'山西', u'陕西', u'四川', u'天津', u'新疆', u'西藏', u'云南', u'浙江']

name_dict = {
    u'new_China': u'new',
    u'sh_China': u'sh'
}

@task(help={
    'name': 'crawler name. Obligatory parameter.',
    'cities': 'city/cities name. To scrape info about several cities add them separated by "," without whitespaces. Optional parameter.',
    'provinces': 'province/provinces name. To scrape info about several provinces add them separated by "," without whitespaces. Optional parameter.',
    'queue': 'province number. Scrape info about provinces one by one. Optional parameter.'
})
def run_crawl_estate_spider(ctx, name, provinces=None, cities=None, queque_provinces=None):
    '''
    :return: info about property in China from http://www.cityhouse.cn/city.html website. Data stored in /.items/ folder.
    '''

    if queque_provinces:
        province_num = get_province_num(queque_provinces, name)

        command_str = ''
        for prov_index in range(province_num, len(provinces_list)):
            prov = provinces_list[prov_index]
            command_str += 'invoke crawl_estate.crawl -n {0} -p {1} && '.format(name, prov)
        command_str = command_str[:-4]
    else:
        command_str = "scrapy crawl {0}".format(name)
        if provinces or cities:
            command_str += ' -a'
        if provinces:
            command_str += ' provinces={0}'.format(provinces)
        if cities:
            command_str += ' cities={0}'.format(cities)

    ctx.run(command_str)

def get_province_num(queque_provinces, name):
    file_name = 'command/.logs/log_end_{}_crawl.txt'.format(name_dict[name])
    if 'f' in queque_provinces:
        try:
            with open(file_name, "w"):
                pass
        except IOError:
            pass

        province_num = int(queque_provinces.replace('f', ''))
    else:
        if int(queque_provinces) < len(provinces_list):
            province_num = int(queque_provinces)
        else:
            province_num = 0
        try:
            with open(file_name, 'r') as f:
                lines = f.read().splitlines()
                last_line = lines[-1]
                last_l_list = last_line.split(' ')

                if last_l_list:
                    for i, el in enumerate(provinces_list):
                        if el == last_l_list[-1]:
                            province_num = i + 1
                            break
        except Exception:
            pass

    if province_num >= len(provinces_list):
        province_num = 0

    return province_num