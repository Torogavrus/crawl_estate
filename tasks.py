import sys

from invoke import Collection
from command.crawl_estate import run_crawl_estate_spider

reload(sys)
sys.setdefaultencoding("utf-8")

crawl_estate_ns = Collection('crawl_estate')
crawl_estate_ns.add_task(run_crawl_estate_spider, name="crawl")

ns = Collection(crawl_estate_ns)
ns.configure({'conflicted': 'scrapy crawl me_China -a cities='})
