# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import os
from crawl_estate.feedexport import CSVkwItemExporter


class CrawlEstatePipeline(object):

    sh_fields_list = [
        'province',
        'city',
        'community',
        'completed',
        'location',
        'category',
        'use',
        'building_type',
        'room',
        'average_price',
        'average_rent',
        'floor_area',
        'building_area',
        'commencement_date',
        'project_progress',
        'longitude',
        'latitude',
        'scrape_date',
        'est_url'
    ]

    new_fields_list = [
        'province',
        'city',
        'community',
        'status',
        'date',
        'avg_price',
        'floor_plan',
        'district',
        'street',
        'street_no',
        'use',
        'type',
        'average_price',
        'average_rent',
        'floor_area',
        'building_area',
        'commencement_date',
        'project_progress',
        'longitude',
        'latitude',
        'scrape_date',
        'est_url'
    ]

    def __init__(self):
        self.files = {}
        self.exporters = {}

    def spider_closed(self, spider):
        [e.finish_exporting() for e in self.exporters.values()]
        [f.close() for f in self.files.values()]

    def process_item(self, item, spider):
        if 'new_China' in spider.name:
            fil_path = '.items/newest/'
            if not os.path.exists(fil_path):
                os.makedirs(fil_path)
            field_list = self.new_fields_list
        else:
            fil_path = '.items/shest/'
            if not os.path.exists(fil_path):
                os.makedirs(fil_path)
            field_list = self.sh_fields_list
        file_name = item['province'] + '_' + item['city']
        if file_name not in self.files:
            self.files[file_name] = open(fil_path + file_name + '.csv','w+b')
            self.exporters[file_name] = CSVkwItemExporter(self.files[file_name], fields_to_export=field_list)
            self.exporters[file_name].start_exporting()

        self.exporters[file_name].export_item(item)
        return item
