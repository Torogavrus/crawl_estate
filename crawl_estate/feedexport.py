from scrapy.conf import settings
from scrapy.exporters import CsvItemExporter

class CSVkwItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        kwargs['encoding'] = settings.get('EXPORT_ENCODING', 'utf-8')
        delimiter = settings.get('CSV_DELIMITER', ',')

        super(CSVkwItemExporter, self).__init__(*args, **kwargs)
