# -*- coding: utf-8 -*

import re
import time
import sys

import scrapy
import logging
from scrapy.utils.log import configure_logging

from crawl_estate.items import CrawlEstateItem


class MEstChinaSpider(scrapy.Spider):
    name = 'new_China'

    custom_settings = {
        'CONCURRENT_REQUESTS': 16,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 16,
        'CONCURRENT_REQUESTS_PER_IP': 16
    }

    configure_logging(install_root_handler=False)
    logging.basicConfig(
        filename='.logs/log.txt',
        format='%(levelname)s: %(message)s',
        level=logging.INFO
    )

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler = logging.FileHandler('.logs/http_errors.txt')
    handler.setFormatter(formatter)

    logger_url = logging.getLogger('urllogger')
    logger_url.addHandler(handler)

    formatter_end = logging.Formatter('%(message)s')
    handler_end = logging.FileHandler('command/.logs/log_end_new_crawl.txt')
    handler_end.setFormatter(formatter_end)

    logger_end_new_crawl = logging.getLogger('end_crawl_logger')
    logger_end_new_crawl.addHandler(handler_end)

    RE_PAGE_NUM = re.compile('(\d*)')
    RE_GEOTAG = re.compile(r"gpsxy\(\'(.*)\'\)\;")

    M_START_PAGE = 'http://m.cityhouse.cn/'

    start_urls = ('http://www.cityhouse.cn/city.html',)

    province_dict = {}
    province_list = []
    cities_list = []


    def __init__(self, provinces=None, cities=None, *args, **kwargs):
        super(MEstChinaSpider, self).__init__(**kwargs)
        self.callback_url = kwargs.get('callback_url', None)
        reload(sys)
        sys.setdefaultencoding("utf-8")
        if provinces:
            self.province_list = provinces.split(',')
        if cities:
            self.cities_list = cities.split(',')

    def parse(self, response):
        '''
        take provinces and cities from site
        :param response:
        :return: province_dict (provinces:cities dict)
        '''
        province_dict = {}
        province_name = ''
        for el in response.xpath('.//div[@id="city_toggle"]//td[@class="right_city"]/span'):
            if 's_province s_plast ordinary_province' in el.xpath('./@class').extract_first():
                province_name = el.xpath('./text()').extract_first().strip()
                province_dict[province_name] = []
            elif el.xpath('./@class').extract_first() == 'm_d_zx':
                province_name = el.xpath('./a/text()').extract_first().strip()
                province_dict[province_name] = [province_name]
            elif el.xpath('./@class').extract_first() == 'wraplist':
                for city_el in el.xpath('./span[@class="wrap"]'):
                    city_name = city_el.xpath('./span[@class="m_d_zx"]/a/text()').extract_first().strip()
                    province_dict[province_name].append(city_name)

        self.province_dict = province_dict

        yield scrapy.Request(url=self.M_START_PAGE, callback=self.parse_mob, dont_filter=True)

    def parse_mob(self, response):
        for count, el in enumerate(response.xpath('.//div[@class="city-sel"]/ul/li')):
            city_name = el.xpath('./a/span/text()').extract_first().strip()
            province_name = self.get_pname(city_name)
            city_url = el.xpath('./a/@href').extract_first()

            if self.province_list and province_name not in self.province_list:
                continue
            if self.cities_list and city_name not in self.cities_list:
                continue

            request = scrapy.Request(city_url, callback=self.parse_city, dont_filter=True)
            request.meta['province_name'] = province_name
            request.meta['city_name'] = city_name
            yield request

    def parse_city(self, response):
        page_num_bl = response.xpath('.//span[@class="mr5"]/span[@class="red"]/text()').extract_first()
        if not page_num_bl:
            self.logger.info("Cann't find number of estate for province {0} city {1}".format(response.meta['province_name'], response.meta['city_name']))
            return

        item_num = page_num_bl.strip().replace(',', '')

        for count, el in enumerate(response.xpath('.//nav[@class="nav-links"]/ul/li')):
            if count != 1:
                continue

            url = 'http://m.cityhouse.cn' + el.xpath('./a/@href').extract_first() + 'AjaxList.html'
            f_data = {
                'listParamsJson[keywords]': '',
                'listParamsJson[district]': '',
                'listParamsJson[hacode]': '',
                'listParamsJson[hadistance]': '',
                'listParamsJson[stationcode]': '',
                'listParamsJson[streetcode]': '',
                'listParamsJson[area_code]': '',
                'listParamsJson[haclcode]': '',
                'listParamsJson[proptype]': '',
                'listParamsJson[bldgcode]': '',
                'listParamsJson[bz_choice]': '',
                'listParamsJson[bz]': '',
                'listParamsJson[urbanid]': '',
                'listParamsJson[p1]': '0',
                'listParamsJson[p2]': '0',
                'listParamsJson[pagesize]': item_num,
                'listParamsJson[all]': '',
                'listParamsJson[gps]': '',
                'listParamsJson[ob]': '0',
                'listParamsJson[page]': '1',
                'listParamsJson[ha_new]': 'new'
            }

            request = scrapy.FormRequest(url, callback=self.parse_est_list, dont_filter=True, formdata=f_data, errback=self.log_unav)
            request.meta['province_name'] = response.meta['province_name']
            request.meta['city_name'] = response.meta['city_name']
            yield request

    def parse_est_list(self, response):
        for count, el in enumerate(response.xpath('.//a[@class="block-a"]')):
            url = 'http://m.cityhouse.cn' + el.xpath('./@href').extract_first()

            request = scrapy.Request(url, callback=self.parse_est_item, dont_filter=True, errback=self.log_unav)
            request.meta['province_name'] = response.meta['province_name']
            request.meta['city_name'] = response.meta['city_name']
            yield request

    def parse_est_item(self, response):
        try:
            est_item = CrawlEstateItem()
            est_item['province'] = response.meta['province_name']
            est_item['city'] = response.meta['city_name']
            est_item['community'] = response.xpath('.//div[@class="title"]/text()').extract_first().strip()

            data_list = response.xpath('.//div[@class="newha-info"]/ul/li')
            est_item['status'] = data_list[0].xpath('./span/text()').extract()[1].strip()

            est_item['date'] = None
            date_val_list = data_list[1].xpath('./span/text()').extract()
            if len(date_val_list) > 2:
                date_list = date_val_list[2].strip()[1:-1].split('-')
                est_item['date'] = '/'.join([date_list[1], date_list[2], date_list[0]])

            est_item['avg_price'] = data_list[1].xpath('./span/span/text()').extract_first().strip().replace(',', '')

            if len(data_list[2].xpath('./span/text()').extract()) > 1:
                est_item['floor_plan'] = data_list[2].xpath('./span/text()').extract()[1].strip()
            else:
                est_item['floor_plan'] = data_list[2].xpath('./a/text()').extract_first().strip()

            data_list_2 = response.xpath('.//div[@class="newha-info"]//ul[@class="mt15"]/li')
            est_item['district'] = data_list_2[2].xpath('./span/a/text()').extract_first().strip()

            est_item['street'] = None
            if len(data_list_2[2].xpath('./span/a')) > 1:
                est_item['street'] = data_list_2[2].xpath('./span/a/text()').extract()[1].strip()

            est_item['street_no'] = None
            if len(data_list_2[2].xpath('./span')) > 3:
                sn = data_list_2[2].xpath('./span')[3].xpath('.//text()')
                if sn and sn.extract_first().strip():
                    est_item['street_no'] = sn.extract_first().strip()

            use_list = data_list_2[3].xpath('./span//text()').extract()[1:]
            est_item['use'] = ' '.join([x.strip() for x in use_list if x.strip()])

            type_list = data_list_2[4].xpath('./span//text()').extract()[1:]
            est_item['type'] = ' '.join([x.strip() for x in type_list if x.strip()])

            price_el_list = response.xpath('.//div[@class="newha-price"]/p')
            est_item['average_price'] = '--'
            if len(price_el_list) >= 2:
                est_item['average_price'] = price_el_list[1].xpath('.//span/span[@class="r-n"]//text()').extract_first().strip().replace(',', '')
            est_item['average_rent'] = '--'
            if len(price_el_list) >= 4:
                est_item['average_rent'] = price_el_list[3].xpath('.//span/span[@class="r-n"]//text()').extract_first().strip().replace(',', '')

            est_item['scrape_date'] = time.strftime("%x")
            est_item['est_url'] = response.url

            base_url_list = response.url.split('/')
            base_url_list.insert(len(base_url_list)-1, 'baseInfo')
            base_url = '/'.join(base_url_list)
            request = scrapy.Request(base_url, callback=self.parse_base_info, dont_filter=True, errback=self.log_unav)
            request.meta['est_item'] = est_item
            request.meta['province_name'] = response.meta['province_name']
            request.meta['city_name'] = response.meta['city_name']
            request.meta['base_url_list'] = response.url.split('/')

            yield request

        except Exception as e:
            est_url = response.url
            if 'redirect_urls' in response.meta.keys():
                est_url = response.meta['redirect_urls'][0]

            self.logger_url.info('Error {0} when scraping url {1} (province: {2}, city: {3})'.format(str(e), est_url, response.meta['province_name'], response.meta['city_name']))

    def parse_base_info(self, response):
        est_item = response.meta['est_item']
        base_url_list = response.meta['base_url_list']

        try:
            base_info_list = ['floor_area', 'building_area', 'commencement_date', 'project_progress']
            for el in response.xpath('.//div[@class="infobox"]'):
                if el.xpath('./div[@class="tit"]/text()').extract_first().strip() == u'更多信息':
                    for key, dat_el in zip(base_info_list, el.xpath('.//li')):
                        base_text_l = dat_el.xpath('./text()').extract_first().split(u'\uff1a')
                        if len(base_text_l) > 1:
                            est_item[key] = base_text_l[1].strip()
                        else:
                            est_item[key] = None
                    break
        except Exception as e:
            est_url = response.url
            if 'redirect_urls' in response.meta.keys():
                est_url = response.meta['redirect_urls'][0]

            self.logger_url.info('Error {0} when scraping base info url {1} '
                                 '(province: {2}, city: {3})'.format(str(e), est_url,
                                                                     response.meta['province_name'],
                                                                     response.meta['city_name']))
        base_url_list.insert(len(base_url_list) - 1, 'zb')
        map_url = '/'.join(base_url_list)

        request = scrapy.Request(map_url, callback=self.parse_map_info, dont_filter=True, errback=self.log_unav)
        request.meta['est_item'] = est_item
        request.meta['province_name'] = response.meta['province_name']
        request.meta['city_name'] = response.meta['city_name']
        request.meta['base_url_list'] = response.meta['base_url_list']

        yield request

    def parse_map_info(self, response):
        est_item = response.meta['est_item']
        # base_url_list = response.meta['base_url_list']
        try:
            geotags_obj = self.RE_GEOTAG.search(response.body)
            if geotags_obj:
                geotags = geotags_obj.group(1).split("','")

                est_item['longitude'] = str(geotags[0])
                est_item['latitude'] = str(geotags[1])
            else:
                est_item['longitude'] = None
                est_item['latitude'] = None
        except Exception as e:
            est_item['longitude'] = None
            est_item['latitude'] = None

            est_url = response.url
            if 'redirect_urls' in response.meta.keys():
                est_url = response.meta['redirect_urls'][0]

            self.logger_url.info('Error {0} when scraping GeoTags url {1} '
                                 '(province: {2}, city: {3})'.format(str(e), est_url, response.meta['province_name'],
                                                                     response.meta['city_name']))
        yield est_item

    def get_pname(self, city_name):
        for k, v in self.province_dict.items():
            for c_n in v:
                if city_name == c_n:
                    return k

    def log_unav(self, response):
        est_url = response.request.url
        if 'redirect_urls' in response.request.meta.keys():
            est_url = response.request.meta['redirect_urls'][0]
        self.logger_url.info('HTTP Error {0} when scraping url {1} (province: {2}, city: {3})'.format(response.request.url, est_url, response.request.meta['province_name'], response.request.meta['city_name']))

    def closed(self, spider):
        if self.province_list:
            self.logger_end_new_crawl.info('New houses - scraped province {}'.format(self.province_list[0].encode('utf-8')))
