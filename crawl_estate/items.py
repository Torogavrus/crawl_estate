# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlEstateItem(scrapy.Item):
    province = scrapy.Field()
    city = scrapy.Field()
    community = scrapy.Field()
    status = scrapy.Field()
    date = scrapy.Field()
    avg_price = scrapy.Field()
    floor_plan = scrapy.Field()
    district = scrapy.Field()
    street = scrapy.Field()
    street_no = scrapy.Field()
    use = scrapy.Field()
    type = scrapy.Field()
    average_price = scrapy.Field()
    average_rent = scrapy.Field()
    scrape_date = scrapy.Field()
    est_url = scrapy.Field()
    floor_area = scrapy.Field()
    building_area = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    commencement_date = scrapy.Field()
    project_progress = scrapy.Field()


class CrawlSHEstateItem(scrapy.Item):
    province = scrapy.Field()
    city = scrapy.Field()
    community = scrapy.Field()
    room = scrapy.Field()
    completed = scrapy.Field()
    location = scrapy.Field()
    category = scrapy.Field()
    use = scrapy.Field()
    building_type = scrapy.Field()
    average_price = scrapy.Field()
    average_rent = scrapy.Field()
    scrape_date = scrapy.Field()
    est_url = scrapy.Field()
    floor_area = scrapy.Field()
    building_area = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
    commencement_date = scrapy.Field()
    project_progress = scrapy.Field()
