#crawl_estate

# Quick Start


## Basics

**1. Run crawler **

    1. To crawl information about buildings

        about new buildings
        '''
        invoke crawl_estate.crawl -n new_China
        '''
        about second-hand buildings
        '''
        invoke crawl_estate.crawl -n sh_China
        '''
        this command scrape all information about new buildings in all provinces and all cities



        parameter '''-n''' is obligatory


**2. To crawl information about one or more cities **

    add parameter '''-c''' and name of city/cities

    for example command
    '''
    invoke crawl_estate.crawl -n new_China -c 临沂
    '''
    will scrape all data about new property in 临沂 city

    to scrape info about several cities - just add them after '''-c''' separated by ''',''' without whitespaces
    for example
    '''
    invoke crawl_estate.crawl -n new_China -c 临沂,福州,莆田
    '''

    parameter '''-c''' is optional


**3. To crawl information about one or more provinces **

    do the same as with cities only specify them with '''-p'''

    for example
    '''
    invoke crawl_estate.crawl -n new_China -p 福建
    '''

    will scrape all info about new property in 福建 province

    '''
    invoke crawl_estate.crawl -n new_China -p 福建,甘肃,广东
    '''
    will scrape all info about new property in provinces 福建, 甘肃 and 广东


**4. Scraping provinces by queue **

    Provinces list:  ["安徽", "北京", "重庆", "福建", "甘肃", "广东", "广西", "贵州", "海南", "河北", "黑龙江", '河南', '湖北',
                      '湖南', '江苏', '江西', '吉林', '辽宁', '内蒙古', '宁夏', '青海', '上海', '山东', '山西', '陕西', '四川',
                      '天津', '新疆', '西藏', '云南', '浙江']

    1. To scrape provinces one by one you should run
        '''
        invoke crawl_estate.crawl -n new_China -q 0
        '''
        and it will start scrape province by province

        if crawler was interrupted (error or by some other reason), you can run the same command and it will continue scrape from last province

        Data about already scraped provinces is storing in files '/command/.logs/log_end_new_crawl.txt' and '/command/.logs/log_end_sh_crawl.txt', for new and for second hand hauses respectively.
        And if you delete them, crawling will start from the first province. (Please, don't delete '.logs' folder)


    2. If you want to scrape province by province but not from first you should run command with province number and letter 'f'
        '''
        invoke crawl_estate.crawl -n new_China -q 5f
        '''
        and it will start crawl province by province starting from the 5-th province 广东 according to list (numerating provinces starts from 0)


**5. Storing scraped data **

    scraped data about new buildings will store in /.items/new_est folder
    scraped data about second-hand buildings will store in /.items/sh_est folder
    name of each file consists with province name and city name
